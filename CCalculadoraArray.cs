﻿using System;
using System.Collections.Generic;
using System.Text;

namespace adapter
{
    class CCalculadoraArray
    {
        public double suma(int[] pOperadores)
        {
            int n = 0;
            double r = 0;

            for (n = 0; n < pOperadores.Length; n++)
                r += pOperadores[n];

            return r;
        }
    }
}
